package com.solutec.laferme.web.controler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.solutec.laferme.dao.entities.Utilisateur;
import com.solutec.laferme.service.lafermemetier;

import com.solutec.laferme.dao.entities.Activité;
import com.solutec.laferme.dao.entities.Animal;
import com.solutec.laferme.dao.entities.Fruit;
import com.solutec.laferme.dao.entities.Legume;
import com.solutec.laferme.dao.entities.Panier;
import com.solutec.laferme.dao.entities.Parrainage;
import com.solutec.laferme.dao.entities.Produit;
import com.solutec.laferme.dao.entities.Reservation;
import com.solutec.laferme.dao.entities.Salade;
import com.solutec.laferme.dao.entities.Viande;
import com.solutec.laferme.dao.repository.AnimalRepository;

@Controller
public class controler {

	@Autowired
	lafermemetier lafermemetier;

	@RequestMapping(value = "/")
	public String accueil() {
		return "Accueil";
	}

	@RequestMapping(value = "/presentation")
	public String presentation() {
		return "presentation";
	}

	@RequestMapping(value = "/gestion")
	public String gestion() {
		return "gestion";
	}

	@RequestMapping(value = "/produit")
	public String produit(Model model) {
		Produit produit = new Produit();
		List<Produit> listeProduit = lafermemetier.getAllProduit();
		Panier panier = new Panier();
		model.addAttribute("listeProduit", listeProduit);
		model.addAttribute("produit", produit);
		model.addAttribute("panier", panier);
		return "produit";
	}

	@RequestMapping(value = "/supprimerProduit")
	public String supprimerProduit(@RequestParam int id) {
		Optional<Produit> opf = lafermemetier.getProduit(id);
		try {
			if (opf.isPresent())
				lafermemetier.deleteProduit(opf.get());
		} catch (Exception e) {
			System.out.println("suppression impossible!!!!");
		}
		return "redirect:/produit";
	}

	@RequestMapping(value = "/ajouterproduit")
	public String ajouterProduit(Model model) {
		Produit produit = new Produit();
		List<Produit> listeproduit = lafermemetier.getAllProduit();
		model.addAttribute("listeproduit", listeproduit);
		model.addAttribute("produit", produit);
		return "ajouterproduit";
	}
	
	
	
	@RequestMapping(value="/parrainage")
	public String animal(Model model) {
		List<Utilisateur> user = lafermemetier.getAllUtilisateur();
		model.addAttribute("userId", user.get(0).getId());
	return "Animal";
	}

	@RequestMapping(value = "/Canard")
	public String canard(Model model) {
		Parrainage parrainage = new Parrainage();
		model.addAttribute("parrainage", parrainage);
		return "Canard";
	}

	@RequestMapping(value = "/Cochon")
	public String cochon(Model model) {
		Parrainage parrainage = new Parrainage();
		model.addAttribute("parrainage", parrainage);
		return "Cochon";
	}

	@RequestMapping(value = "/Coq")
	public String coq(Model model) {
		Parrainage parrainage = new Parrainage();
		model.addAttribute("parrainage", parrainage);
		return "Coq";
	}

	@RequestMapping(value = "/Poney")
	public String poney(Model model) {
		Parrainage parrainage = new Parrainage();
		model.addAttribute("parrainage", parrainage);
		return "Poney";
	}

	@RequestMapping(value = "/Vache")
	public String vache(Model model) {
		Parrainage parrainage = new Parrainage();
		model.addAttribute("parrainage", parrainage);
		return "Vache";
	}

	@RequestMapping(value = "/Lapin")
	public String lapin(Model model) {
		Parrainage parrainage = new Parrainage();
		model.addAttribute("parrainage", parrainage);
		return "Lapin";
	}

	@RequestMapping(value = "/Brebis")
	public String brebis(Model model) {
		Parrainage parrainage = new Parrainage();
		model.addAttribute("parrainage", parrainage);
		return "Brebis";
	}


	@PostMapping(value = "/ajouterproduit")
	public String ajouterproduit(Produit produit) {
		lafermemetier.saveProduit(produit);
		return "redirect:/produit";
	}

	@RequestMapping(value = "/activite")
	public String activite(Model model) {
		Activité activite = new Activité();
		List<Activité> listeactivite = lafermemetier.findAllActivités();
		model.addAttribute("listeactivite", listeactivite);
		model.addAttribute("activite", activite);
		return "activite";
	}

	@RequestMapping(value = "/ajouteractivite")
	public String ajouterActivite(Model model) {
		Activité activite = new Activité();
		List<Activité> listeactivite = lafermemetier.findAllActivités();
		model.addAttribute("listeactivite", listeactivite);
		model.addAttribute("activite", activite);
		return "ajouteractivite";
	}

	@PostMapping(value = "/ajouteractivite")
	public String ajouterActivite(Activité activité) {
		lafermemetier.saveActivite(activité);
		return "redirect:/activite";
	}

	@RequestMapping(value = "/gestionReservation")
	public String gestionreservation() {
		return "gestionReservation";
	}

	@RequestMapping(value = "/ajouterReservation")
	public String ajouterReservation(Model model) {
		Reservation reservation = new Reservation();
		List<Reservation> listeReservation = lafermemetier.findAllReservation();
		model.addAttribute("listeReservation", listeReservation);
		model.addAttribute("reservation", reservation);
		return "ajouterReservation";
	}

	@PostMapping(value = "/ajouterReservation")
	public String ajouterReservation(Reservation reservation) {
		lafermemetier.saveReservation(reservation);
		return "redirect:/gestionReservation";
	}

	// -----Reservation------

	@RequestMapping(value = "/reservation")
	public String reservation(Model model) {
		List<Utilisateur> user = lafermemetier.getAllUtilisateur();
		model.addAttribute("userId", user.get(0).getId());
		return "reservation";
	}
	@RequestMapping(value = "/presactivite")
	public String presactivite(Model model) {
		return "presentation";
	}

	@RequestMapping(value = "/formulairereservation")
	public String formulairereservation(Model model) {
		Reservation res = new Reservation();
		List<Activité> acts = lafermemetier.findAllActivités();
		model.addAttribute("activit", acts);
		model.addAttribute("res", res);
		return "formulairereservation";
	}

	@RequestMapping(value = "/affichereservation")
	public String affichereservation(Model model) {
		List<Reservation> res = lafermemetier.findAllReservation();
		Reservation rese = res.get(res.size() - 1);
		model.addAttribute("rese", rese);
		return "affichereservation";
	}

	@PostMapping(value = "/postreservation")
	public String postreservation(Reservation r) {
		lafermemetier.saveReservation(r);
		return "redirect:affichereservation";
	}

	@PostMapping(value = "/postreservationmodif")
	public String postreservationmodif(Reservation r) {
		lafermemetier.saveReservation(r);
		return "redirect:reservation";
	}

	@RequestMapping(value = "/gestionuserreservation")
	public String gestionuserreservation(Model model, @RequestParam(name = "id") int id) {
		List<Reservation> listereservation = lafermemetier.findAllReservation();
		List<Reservation> reservation = new ArrayList<Reservation>();
		for (Reservation r : listereservation) {
			if (r.getUtilisateur().getId() == id) {
				reservation.add(r);
			}
		}

		model.addAttribute("reservation", reservation);
		return "gestionuserreservation";
	}

	@RequestMapping(value = "/supprimeruserReservation")
	public String supprimeruserReservation(@RequestParam int id) {
		Optional<Reservation> opf = lafermemetier.getReservation(id);
		try {
			if (opf.isPresent())
				lafermemetier.deleteReservation(opf.get());
		} catch (Exception e) {
			System.out.println("suppression impossible!!!!");
		}
		return "redirect:/reservation";
	}

	@RequestMapping(value = "/modifieruserReservation")
	public String modifieruserReservation(Model model, @RequestParam(name = "id") int id) {
		Optional<Reservation> reservation = lafermemetier.getReservation(id);
		List<Activité> acts = lafermemetier.findAllActivités();
		model.addAttribute("activit", acts);
		model.addAttribute("res", reservation.get());

		return "modificationformulairereservation";
	}

	@RequestMapping(value = "/utilisateur")
	public String utilisateur(Model model) {
		Utilisateur user = new Utilisateur();
		List<Utilisateur> listeUtilisateur = lafermemetier.getAllUtilisateur();
		model.addAttribute("listeUtilisateur", listeUtilisateur);
		model.addAttribute("user", user);
		return "utilisateur";

	}

	@RequestMapping(value = "/ajouterutilisateur")
	public String ajouterutilisateur(Model model) {
		Utilisateur user = new Utilisateur();
		List<Utilisateur> listeUtilisateur = lafermemetier.getAllUtilisateur();
		model.addAttribute("listeUtilisateur", listeUtilisateur);
		model.addAttribute("user", user);
		return "ajouterutilisateur";

	}

	@PostMapping(value = "/ajouterutilisateur")
	public String ajouterutilisateur(Utilisateur utilisateur) {
		lafermemetier.saveUtilisateur(utilisateur);
		return "redirect:/utilisateur";
	}

	@RequestMapping(value = "/modifierUtilisateur")
	public String modifierUtilisateur(@RequestParam(name = "id") int id, Model model) {
		Optional<Utilisateur> oputilisateur = lafermemetier.getUtilisateur(id);
		Utilisateur utilisateur = oputilisateur.get();
		model.addAttribute("utilisateur", utilisateur);
		return "modifierutilisateur";
	
	}

	@PostMapping(value = "/modifierutilisateur")
	public String majUtilisateur(Utilisateur utilisateur) {
		lafermemetier.saveUtilisateur(utilisateur);
		return "redirect:/utilisateur";
	}

	@RequestMapping(value = "/supprimerUtilisateur")
	public String supprimerUtilisateur(@RequestParam int id) {
		Optional<Utilisateur> opf = lafermemetier.getUtilisateur(id);
		try {
			if (opf.isPresent())
				lafermemetier.deleteUtilisateur(opf.get());
		} catch (Exception e) {
			System.out.println("suppression impossible!!!!");
		}
		return "redirect:/utilisateur";
	}

// ----- Achat ------	
	@RequestMapping(value = "/achat")
	public String achat() {
		return "pagedachat";
	}

// ------ Viande ------
	@RequestMapping("/viandes")
	public String viandes_page(Model model, @RequestParam(name = "num_page", defaultValue = "0") int numPage) {
		List<Viande> listViandes = lafermemetier.getAllViande();
		model.addAttribute("listeViandes", listViandes);
//		int np = listViandes.getTotalPages();
//		int[] t = new int[np];
//		for (int i = 0; i < np; i++) {
//			t[i] = i;
//		}
//		model.addAttribute("pages", t);
		return "achatviandes";
	}

	@RequestMapping("/ajouterViande")
	public String ajouterViande(@RequestParam(name = "idv") int idv) {
		Optional<Viande> opv = lafermemetier.getViande(idv);
		try {
//			if (opv.isPresent())

		} catch (Exception e) {
			System.out.println("Ajout impossible");
		}
		return "redirect:/user/formulaireViande";
	}

	@RequestMapping("/formulaireViande")
	public String formulaireViande() {
		return ("/viandes");
	}

// ----- Légume -----
	@RequestMapping("/legumes")
	public String legumes_page(Model model, @RequestParam(name = "num_page", defaultValue = "0") int numPage) {
		List<Legume> listLegumes = lafermemetier.getAllLegume();
		model.addAttribute("listeLegumes", listLegumes);
		return "achatlegumes";
	}

	@RequestMapping("/ajouterLegume")
	public String ajouterLegume(@RequestParam(name = "idv") int idv) {
		try {
//			if (opv.isPresent())
		} catch (Exception e) {
			System.out.println("Ajout impossible");
		}
		return "redirect:/formulaireLegume";
	}

	@RequestMapping("/formulaireLegume")
	public String formulaireLegume() {

		return "/legumes";
	}

	// ----- Salade -----
	@RequestMapping("/salades")
	public String Salades_page(Model model, @RequestParam(name = "num_page", defaultValue = "0") int numPage) {
		List<Salade> listSalades = lafermemetier.getAllSalade();
		model.addAttribute("listeSalades", listSalades);
		return "achatsalades";
	}

	@RequestMapping("/ajouterSalade")
	public String ajouterSalade(@RequestParam(name = "idv") int idv) {
		try {
//				if (opv.isPresent())
		} catch (Exception e) {
			System.out.println("Ajout impossible");
		}
		return "/formulaireSalade";
	}

	@RequestMapping("/formulaireSalade")
	public String formulaireSalade() {
		return ("/");
	}

	// ----- Légume -----
	@RequestMapping("/fruits")
	public String fruits_page(Model model, @RequestParam(name = "num_page", defaultValue = "0") int numPage) {
		List<Fruit> listFruits = lafermemetier.getAllFruit();
		model.addAttribute("listeFruits", listFruits);
		return "achatFruits";
	}

	@RequestMapping("/ajouterFruit")
	public String ajouterFruit(@RequestParam(name = "idv") int idv) {
		try {
//				if (opv.isPresent())
		} catch (Exception e) {
			System.out.println("Ajout impossible");
		}
		return "/formulaireFruit";
	}

	@RequestMapping("/user/formulaireFruit")
	public String formulaireFruit() {
		return ("/");
	}

// ----- Parrainage -----	

	@PostMapping(value = "/ajouterparrainage")
	public String ajouterparrainage(Parrainage p) {
		lafermemetier.saveParrainage(p);
		return "redirect:/parrainage";
	}
	
	
	@RequestMapping({ "/afficheparrainage" })
	public String parrainage(Model model, @RequestParam(name="id") int id) {
		List<Parrainage> liste = lafermemetier.findAllParrainage();
		List<Parrainage> listeparrainage = new ArrayList<Parrainage>();
		
		for(Parrainage par : liste) {
			if(par.getIdutil() == id) {
				listeparrainage.add(par);
			}
		}

		model.addAttribute("listeParrainages", listeparrainage);
		return "afficheparrainage";
	}
	
	
	
	
//	@RequestMapping(value = "/gestionuserreservation")
//	public String gestionuserreservation(Model model, @RequestParam(name = "id") int id) {
//		List<Reservation> listereservation = lafermemetier.findAllReservation();
//		List<Reservation> reservation = new ArrayList<Reservation>();
//		for (Reservation r : listereservation) {
//			if (r.getUtilisateur().getId() == id) {
//				reservation.add(r);
//			}
//		}
//
//		model.addAttribute("reservation", reservation);
//		return "gestionuserreservation";
//	}
	
	

	@RequestMapping("/modifierProduit")
	public String editProduit(@RequestParam(name = "id") int id, Model model) {
		Optional<Produit> opproduit = lafermemetier.getProduit(id);

		if (opproduit.get().getClass().getSimpleName().equals("Viande")) {
			Viande viande = (Viande) opproduit.get();
			model.addAttribute("viande", viande);
			return "edit_viande";
		}
		
		if (opproduit.get().getClass().getSimpleName().equals("Fruit")) {
			Fruit fruit = (Fruit) opproduit.get();
			model.addAttribute("fruit", fruit);
			return "edit_fruit";
		}
		
		if (opproduit.get().getClass().getSimpleName().equals("Legume")) {
			
			Legume legume = (Legume) opproduit.get();
			model.addAttribute("legume", legume);
			return "edit_legume";
		}
		else if (opproduit.get().getClass().getSimpleName().equals("Salade")) {
			Salade salade = (Salade) opproduit.get();
			model.addAttribute("salade", salade);
			return "edit_salade";
		}
		
		return "modifierproduit";
	}

	@PostMapping("/postproduit")
	public String majProduit(Produit produit) {
		lafermemetier.saveProduit(produit);
		return "redirect:/produit";
	} 


//----- Panier -----
	@PostMapping("postviande")
	public String majViande(Viande viande) {
		lafermemetier.saveViande(viande);
		return "redirect:/produit";
	}
	
	@PostMapping("postfruit")
	public String majFruit(Fruit fruit) {
		lafermemetier.saveFruit(fruit);
		return "redirect:/produit";
	}

//	@RequestMapping(value = "/formulairereservation")
//	public String formulairereservation(Model model) {
//		Reservation res = new Reservation();
//		List<Activité> acts = lafermemetier.findAllActivités();
//		model.addAttribute("activit", acts);
//		model.addAttribute("res", res);
//		return "formulairereservation";
//	}
//----- Panier -----
	
	@PostMapping("postlegume")	
	public String majLegume(Legume legume) {
		lafermemetier.saveLegume(legume);
		return "redirect:/produit";
	}
	
	@PostMapping("postsalade")
	public String majSalade(Salade salade) {
		lafermemetier.saveSalade(salade);
		return "redirect:/produit";
	}
	
	

//----- Panier -----

	@RequestMapping(value = "/panier")
	public String panier() {
		return "Panier";
	}

	@RequestMapping(value = "/ajouterproduitpanier")
	public void ajouterProduitPanier(Model model, Integer idp) {
		Optional<Produit> produit = lafermemetier.getProduit(idp);
		Optional<Panier> p = Optional.of(new Panier());
//		p.get().getPanier(idp).getProduit()
//		.add(produit);
		Optional<Panier> pa = lafermemetier.getPanier(idp);
		System.out.println(pa.get().getProduits());

	}

// ----- Gestion -----
	@Autowired
	AnimalRepository animalrepository;

	@RequestMapping(value = "/gestionanimal")
	public String gestionanimal(Model model) {
		Animal animal = new Animal();
		List<Animal> listeAnimal = lafermemetier.getAllAnimal();
		model.addAttribute("listeAnimal", listeAnimal);
		model.addAttribute("animal", animal);
		return "gestionanimal";

	}
}
