package com.solutec.laferme;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import com.solutec.laferme.dao.entities.*;
import com.solutec.laferme.dao.repository.*;


@SpringBootApplication
public class LaFermeApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(LaFermeApplication.class, args);
		UtilisateurRepository utilreposit = ctx.getBean(UtilisateurRepository.class);
		Utilisateur util1 = new Utilisateur(1, "Dupont", "Jean", "Paris","iPhone", "Homme",null,null);
		utilreposit.save(util1);
		Utilisateur util2 = new Utilisateur(2, "Dubois", "Mohamed", "Paris","Samsung", "Homme",null,null);
		utilreposit.save(util2);
		List<Utilisateur> listeUtilisateur = Arrays.asList(util1, util2);
		ActivitéRepository activitéRepository = ctx.getBean(ActivitéRepository.class);
		List<Activité> act = new ArrayList<>();
		Activité act1 = new Activité();
		Activité act2 = new Activité();
		Activité act3 = new Activité();
		Activité act4 = new Activité();
		act1.setNom("visite guidee");
		act2.setNom("cueillette");
		act3.setNom("nourriture");
		act4.setNom("potager");
		act.add(act1);
		act.add(act2);
		act.add(act3);
		act.add(act4);
		activitéRepository.save(act1);
		activitéRepository.save(act2);
		activitéRepository.save(act3);
		activitéRepository.save(act4);
		
		AnimalRepository animalRepository = ctx.getBean(AnimalRepository.class);
		Animal canard = new Animal(1, "Rodé", "canard", " ", " ",null);
		Animal cochon = new Animal(2, "Maxence", "cochon", " ", " ",null);
		Animal coq = new Animal(3, "Dramane", "coq", " ", " ",null);
		Animal vache = new Animal(4, "Marine", "vache", " ", " ",null);
		Animal poney = new Animal(5, "Valentin", "poney", " ", " ",null);
		Animal lapin = new Animal(6, "Audrey", "lapin", " ", " ",null);
		Animal brebis = new Animal(7, "Magaly", "brebis", " ", " ",null);
		animalRepository.save(canard);
		animalRepository.save(cochon);
		animalRepository.save(coq);
		animalRepository.save(vache);
		animalRepository.save(poney);
		animalRepository.save(lapin);
		animalRepository.save(brebis);
		
//		ParrainageRepository parrainageRepository = ctx.getBean(ParrainageRepository.class);
//		Parrainage p1 = new Parrainage(1,1,2,null,24,"rodé",null, null);
//		parrainageRepository.save(p1);
		
		
		
		
		ProduitRepository produitrepository = ctx.getBean(ProduitRepository.class);
		Viande prod1 = new Viande(1, "aile de canard", 10, "viande", 20, 0, "canard", "aile");
		Viande prod2 = new Viande(2, "cote de boeuf", 15, "viande", 50, 0, "boeuf", "cote");
		Salade prod3 = new Salade(3, "Roquette", 150, "salade", 54,0, 200);
		Fruit prod4 = new Fruit(4, "Banane", 3, "fruit", 45, 80,0);
		Legume prod5 = new Legume(5, "Carotte", 2.5, "legume", 54,0);
		produitrepository.save(prod1);
		produitrepository.save(prod2);
		produitrepository.save(prod3);
		produitrepository.save(prod4);
		produitrepository.save(prod5);
	//	Reservation res1 = new Reservation(0, null,"2","Mardi 31 mai 2022","1",act, util2);
	//	ReservationRepository reservationrepository = ctx.getBean(ReservationRepository.class);
		List<Produit> listeProduit = Arrays.asList(prod1, prod2, prod3,prod4,prod5);
	//	reservationrepository.save(res1);
		
		
		
		
		
	}
}
