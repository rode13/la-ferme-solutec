package com.solutec.laferme.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.solutec.laferme.dao.entities.Activité;
import com.solutec.laferme.dao.entities.Animal;
import com.solutec.laferme.dao.entities.Fruit;

import com.solutec.laferme.dao.entities.Legume;
import com.solutec.laferme.dao.entities.Panier;
import com.solutec.laferme.dao.entities.Parrainage;
import com.solutec.laferme.dao.entities.Produit;
import com.solutec.laferme.dao.entities.Reservation;
import com.solutec.laferme.dao.entities.Salade;
import com.solutec.laferme.dao.repository.ActivitéRepository;
import com.solutec.laferme.dao.repository.AnimalRepository;
import com.solutec.laferme.dao.repository.ParrainageRepository;
import com.solutec.laferme.dao.repository.ProduitRepository;
import com.solutec.laferme.dao.repository.ReservationRepository;
import com.solutec.laferme.dao.repository.UtilisateurRepository;
import com.solutec.laferme.dao.repository.ViandeRepository;
import com.solutec.laferme.dao.repository.FruitRepository;
import com.solutec.laferme.dao.repository.LegumeRepository;
import com.solutec.laferme.dao.repository.PanierRepository;
import com.solutec.laferme.dao.repository.SaladeRepository;
import com.solutec.laferme.dao.entities.Reservation;

import com.solutec.laferme.dao.entities.Utilisateur;
import com.solutec.laferme.dao.entities.Viande;


@Service
public class lafermemetierImpl implements lafermemetier {

	@Autowired
	ReservationRepository reservationRepository;
	@Autowired
	ActivitéRepository activiteRepository;

	@Autowired 
	ParrainageRepository parrainageRepository;
	
	@Autowired
	ViandeRepository viandeRepository;
	
	@Autowired
	AnimalRepository animalRepository;
	

	@Autowired
	FruitRepository fruitRepository;
	
	@Autowired
	LegumeRepository legumeRepository;
	
	@Autowired
	SaladeRepository saladeRepository;
	
	@Autowired
	PanierRepository panierrepository;
	
	@Override
	public void saveReservation(Reservation reservation) {
		// TODO Auto-generated method stub
		reservationRepository.save(reservation);

	}

	@Override
	public List<Reservation> findAllReservation() {
		// TODO Auto-generated method stub
		return reservationRepository.findAll();
	}
	
	@Override
	public void deleteReservation(Reservation r) {
		// TODO Auto-generated method stub
		reservationRepository.delete(r);
	}
	
	@Override
	public Optional<Reservation> getReservation(Integer id) {
		// TODO Auto-generated method stub
		return reservationRepository.findById(id);
	}
	
	

	@Autowired
	private UtilisateurRepository utilisateurrepository;

	@Override
	public List<Utilisateur> getAllUtilisateur() {
		// TODO Auto-generated method stub
		return utilisateurrepository.findAll();
	}
	
	@Autowired
	private ProduitRepository produitrepository;
	
	@Override
	public List<Produit> getAllProduit(){
		return produitrepository.findAll();
	}
	
	@Override
	public void saveProduit(Produit produit) {
		produitrepository.save(produit);
	}
	
	@Override
	public Optional<Produit> getProduit(Integer id) {

		return produitrepository.findById(id);
	}

	@Override
	public void deleteProduit(Produit produit) {
		// TODO Auto-generated method stub
		produitrepository.delete(produit);
	}

	@Override
	public void saveActivite(Activité act) {
		// TODO Auto-generated method stub
		activiteRepository.save(act);
		
	}


	@Override
	public List<Activité> findAllActivités() {
		// TODO Auto-generated method stub
		return activiteRepository.findAll();
	}
	
	
	
// ------Parrainage -------
	
	
	@Override
	public void saveParrainage(Parrainage p) {
		parrainageRepository.save(p);
	}
	
	@Override
	public List<Parrainage> findByIdutil(int id_utilisateur){
		return parrainageRepository.findByIdutil(id_utilisateur);
	}
	
	public List<Parrainage> findAllParrainage(){
		return parrainageRepository.findAll();
	}
	
	
	
	
	

	@Override
	public void saveUtilisateur(Utilisateur utilisateur) {
		utilisateurrepository.save(utilisateur);
	}

	
	
// ----- Viandes -----
	


	@Override
	public Optional<Viande> getViande(int idv) {
		// TODO Auto-generated method stub
		return viandeRepository.findById(idv);
	}

	@Override
	public List<Viande> getAllViande() {
		// TODO Auto-generated method stub
		try {
			return viandeRepository.findAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public void saveViande(Viande viande) {
		viandeRepository.save(viande);
	}

	
	
	@Override
	public Optional<Utilisateur> getUtilisateur(Integer id) {
		return utilisateurrepository.findById(id);
	}

	@Override
	public void deleteUtilisateur(Utilisateur utilisateur) {
		// TODO Auto-generated method stub
		utilisateurrepository.delete(utilisateur);
	}


// ----- Legumes -----

	@Override
	public Optional<Legume> getLegume(int idl) {
		// TODO Auto-generated method stub
		return legumeRepository.findById(idl);
	}

	@Override
	public List<Legume> getAllLegume() {
		// TODO Auto-generated method stub
		try {
			return legumeRepository.findAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
		
	public void saveLegume(Legume legume) {
		legumeRepository.save(legume);
	}
	
// ----- Fruits -----

	@Override
	public Optional<Fruit> getFruit(int idf) {
		// TODO Auto-generated method stub
		return fruitRepository.findById(idf);
	}

	@Override
	public List<Fruit> getAllFruit() {
		// TODO Auto-generated method stub
		try {
			return fruitRepository.findAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public void saveFruit(Fruit fruit) {
		fruitRepository.save(fruit);
	}

// ----- Salades -----

	@Override
	public Optional<Salade> getSalade(int ids) {
		// TODO Auto-generated method stub
		return saladeRepository.findById(ids);
	}

	@Override
	public List<Salade> getAllSalade() {
		// TODO Auto-generated method stub
		try {
			return saladeRepository.findAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public void saveSalade(Salade salade) {
		saladeRepository.save(salade);
	}
	
// ----- Panier -----

	@Override
	public Optional<Panier> getPanier(int idp) {
		// TODO Auto-generated method stub
		return panierrepository.findById(idp);
	}

	@Override
	public void savePanier(Panier p) {
		// TODO Auto-generated method stub

		panierrepository.save(p); 

	}


	@Override
	public List<Animal> getAllAnimal() {
		// TODO Auto-generated method stub
		return animalRepository.findAll();
	}
	
	public void saveAnimal(Animal animal) {
		// TODO Auto-generated method stub
		animalRepository.save(animal);

	}
		
		
}
