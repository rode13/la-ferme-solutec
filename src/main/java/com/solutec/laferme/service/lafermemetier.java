package com.solutec.laferme.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;

import com.solutec.laferme.dao.entities.Activité;
import com.solutec.laferme.dao.entities.Animal;
import com.solutec.laferme.dao.entities.Fruit;
import com.solutec.laferme.dao.entities.Legume;
import com.solutec.laferme.dao.entities.Panier;
import com.solutec.laferme.dao.entities.Parrainage;
import com.solutec.laferme.dao.entities.Produit;
import com.solutec.laferme.dao.entities.Reservation;
import com.solutec.laferme.dao.entities.Salade;
import com.solutec.laferme.dao.entities.Utilisateur;
import com.solutec.laferme.dao.entities.Viande;

public interface lafermemetier {

// ----- Utilisateur -----	
	public List<Utilisateur> getAllUtilisateur();

	public void saveUtilisateur(Utilisateur utilisateur);

	public Optional<Utilisateur> getUtilisateur(Integer id);

	public void deleteUtilisateur(Utilisateur utilisateur);

//----- Produit -----	

	public List<Produit> getAllProduit();

	public void saveProduit(Produit produit);

	public Optional<Produit> getProduit(Integer id);

	public void deleteProduit(Produit produit);
	

// ------ Reservation ------
	public void saveReservation(Reservation reservation);

	public List<Reservation> findAllReservation();

	public void saveActivite(Activité act);

	public List<Activité> findAllActivités();
	
	public void deleteReservation(Reservation r);
	
	public Optional<Reservation> getReservation(Integer id);

// ----- Viande -----	

	public Optional<Viande> getViande(int idv);

	public List<Viande> getAllViande();
	
	public void saveViande(Viande viande);

// ----- Légume -----
	public Optional<Legume> getLegume(int idl);

	public List<Legume> getAllLegume();
	
	public void saveLegume(Legume legume);

// ----- Fruit -----
	public Optional<Fruit> getFruit(int idf);

	public List<Fruit> getAllFruit();
	
	public void saveFruit(Fruit fruit);

// ----- Salades -----
	public Optional<Salade> getSalade(int ids);

	public List<Salade> getAllSalade();
	
	public void saveSalade(Salade salade);

	
	
// ----- Parrainage -----
	public void saveParrainage(Parrainage p);
	
//----- Panier -----
	public Optional<Panier> getPanier(int idp);
	
	public void savePanier(Panier p);

	public List<Animal> getAllAnimal();
	public void saveAnimal(Animal animal);

	public List<Parrainage> findByIdutil(int id_utilisateur);

	public List<Parrainage> findAllParrainage();

}


