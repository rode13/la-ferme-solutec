package com.solutec.laferme.dao.entities;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.Id;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
 

public class Viande extends Produit{
	private String animal;
	private String morceau;
	
	public Viande(int id, String libelle, double prix, String cat, int qttes, int qttec, String animal, String morceau) {
		super(id, libelle, prix, cat, qttes, qttec);
		this.animal=animal;
		this.morceau=morceau;
	}
}
