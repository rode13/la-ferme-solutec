package com.solutec.laferme.dao.entities;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.Id;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Fruit extends Produit{
	
	private int pdsuni;
	public Fruit(int id, String libelle, double prix, String cat, int qttes, int qttec, int pdsuni) {
		super(id, libelle, prix, cat, qttes, qttec);
		this.pdsuni=pdsuni;
	}

}
