package com.solutec.laferme.dao.entities;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Parrainage {
		@Id
		@GeneratedValue
		private int id;
		private int idutil;
		private int id_animal;
		private Date date;
		private double montant;
		private String nom_animal;
		
		@ManyToOne  
		private Utilisateur utilisateur;
		
		@ManyToOne 
		private Animal animal;
		
	
}
