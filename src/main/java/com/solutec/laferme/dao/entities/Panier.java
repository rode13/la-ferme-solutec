package com.solutec.laferme.dao.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Panier implements Serializable{
	
	@Id
	@GeneratedValue
	private int id;
	@OneToMany
	private Collection<Produit> produits;
	@OneToOne
	private Utilisateur utilisateur;
	
}	
	
