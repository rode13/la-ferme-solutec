package com.solutec.laferme.dao.entities;

import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.solutec.laferme.dao.entities.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Utilisateur {
	
	@Id @GeneratedValue
	private int id;
	private String nom;
	private String prenom;
	private String adresse;
	private String telephone;
	private String sexe;
	
	@OneToMany
	private List<Reservation> reservation;
	
	@OneToMany
	private Collection<Parrainage> ListParrainage;
	

}
