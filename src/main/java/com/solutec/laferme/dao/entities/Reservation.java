package com.solutec.laferme.dao.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Reservation implements Serializable{
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	String duree;
	String date;
	String nb_adt;
	String nb_enf;
	//String username;

	@ManyToMany 
	@JoinTable(name = "activite_reservation", joinColumns=@JoinColumn(name = "reservation_id"), inverseJoinColumns=@JoinColumn(name = "activite_id"))
	private List<Activité> activite;
	
	@ManyToOne
	private Utilisateur utilisateur;

}
