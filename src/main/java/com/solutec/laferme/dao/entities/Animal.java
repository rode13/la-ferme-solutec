package com.solutec.laferme.dao.entities;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.solutec.laferme.dao.entities.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Animal {
	
	@Id
	private int id;
	private String nom;
	private String espece;
	private String description;
	private String cheminImage;
	
	@OneToMany
	Collection<Parrainage> ListParrainage;
	
	
	
}
