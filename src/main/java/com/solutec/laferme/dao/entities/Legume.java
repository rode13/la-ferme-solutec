package com.solutec.laferme.dao.entities;

import javax.persistence.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor

public class Legume extends Produit{
	public Legume(int id, String libelle, double prix, String cat, int qttes, int qttec) {
		super(id, libelle, prix, cat, qttes, qttec);
	}
}
