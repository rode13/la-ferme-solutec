package com.solutec.laferme.dao.entities;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Produit {
	
	@Id @GeneratedValue
	private int id;
	private String libelle;
	private double prix;
	private String categorie;
	private int quantite_stock;
	private int quantite;
	
	

}
