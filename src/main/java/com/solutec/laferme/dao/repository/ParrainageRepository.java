package com.solutec.laferme.dao.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.solutec.laferme.dao.entities.Parrainage;

public interface ParrainageRepository extends JpaRepository<Parrainage, Integer>{
	
	public List<Parrainage> findByIdutil(int id_utilisateur);
}
