package com.solutec.laferme.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.solutec.laferme.dao.entities.Activité;

public interface ActivitéRepository extends JpaRepository<Activité, Integer>{

}
