package com.solutec.laferme.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.solutec.laferme.dao.entities.Utilisateur;

public interface UtilisateurRepository extends JpaRepository<Utilisateur, Integer>{

}
