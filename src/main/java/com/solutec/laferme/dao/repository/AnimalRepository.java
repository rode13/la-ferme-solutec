package com.solutec.laferme.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.solutec.laferme.dao.entities.Animal;

public interface AnimalRepository extends JpaRepository<Animal, Integer>{

}
