package com.solutec.laferme.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.solutec.laferme.dao.entities.Viande;

public interface ViandeRepository extends JpaRepository<Viande, Integer>{

}
