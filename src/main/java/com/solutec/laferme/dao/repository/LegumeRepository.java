package com.solutec.laferme.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.solutec.laferme.dao.entities.Legume;

public interface LegumeRepository extends JpaRepository<Legume, Integer>{

}
