package com.solutec.laferme.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.solutec.laferme.dao.entities.Panier;

public interface PanierRepository extends JpaRepository<Panier, Integer>{
	

}
