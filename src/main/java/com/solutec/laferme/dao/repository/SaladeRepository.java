package com.solutec.laferme.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.solutec.laferme.dao.entities.Salade;

public interface SaladeRepository extends JpaRepository<Salade, Integer>{

}
