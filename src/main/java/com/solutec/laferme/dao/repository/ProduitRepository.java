package com.solutec.laferme.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.solutec.laferme.dao.entities.Produit;

public interface ProduitRepository extends JpaRepository<Produit, Integer>{

}
