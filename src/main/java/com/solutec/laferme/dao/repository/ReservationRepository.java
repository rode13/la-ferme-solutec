package com.solutec.laferme.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.solutec.laferme.dao.entities.Reservation;

public interface ReservationRepository extends JpaRepository<Reservation,Integer>{

}
