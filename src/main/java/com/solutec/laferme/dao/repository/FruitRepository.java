package com.solutec.laferme.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.solutec.laferme.dao.entities.Fruit;

public interface FruitRepository extends JpaRepository<Fruit, Integer>{

}
